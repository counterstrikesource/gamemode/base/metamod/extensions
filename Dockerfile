FROM registry.gitlab.com/counterstrikesource/sourcemod:latest

# Author
LABEL maintainer="maxime1907 <maxime1907.dev@gmail.com>"

COPY . /home/alliedmodders/sourcemod/extensions

RUN mv /home/alliedmodders/sourcemod/extensions/build_extensions.sh /home/alliedmodders/sourcemod/build_extensions.sh

WORKDIR /home/alliedmodders/sourcemod/

RUN bash /home/alliedmodders/sourcemod/build_extensions.sh css
